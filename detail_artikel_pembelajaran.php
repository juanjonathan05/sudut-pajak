<!DOCTYPE html>
<html lang="en">

<head>
    <title>Artikel Pembelajaran PPh</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <?php
    session_start();
    ?>
    <!-- Header Section Start-->
    <?php include 'konsultasi/navbar4.php'; ?>
    <!-- Header Menu End -->

    <!-- Breadcrumbs Start -->
    <div class="breadcrumbs">
        <div class="breadcrumbs-wrap">
            <img src="images/bg.jpg" alt="Breadcrumbs Image">
            <div class="breadcrumbs-inner">
                <div class="container">
                    <div class="breadcrumbs-text">
                        <h1 class="breadcrumbs-title mb-17">Detail Artikel Pembelajaran PPh</h1>
                        <div class="categories">
                            <ul>
                                <li><a href="index.php"><i class="fa fa-house"></i>Beranda</a></li>
                                <li><a href="pembelajaran.php"><i class="fa fa-house"></i>Pembelajaran PPh</a></li>
                                <li><a href="artikel_pembelajaran.php"><i class="fa fa-house"></i>Artikel Pembelajaran PPh</a></li>
                                <li>Detail Artikel Pembelajaran PPh</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Breadcrumbs End -->
    <!-- Privacy Policy Start -->
    <div class="neuron-about gray-bg pt-90 pb-100 md-pt-70 md-pb-80">
        <div class="container">
            <div class="row col-20 d-flex justify-content-center">
                <div class="col-lg-12 col-md-12 mb-40">
                    <div class="card2">
                        <article>
                            <h2>Pengaruh Pajak Penghasilan (PPh) terhadap Cash Flow UMKM dalam Industri Fashion</h2><br>
                            <img class="thumb p-5" src="https://indonesiafashionweek.id/wp-content/uploads/2020/11/123553863_3453752961370211_3707274402065282295_n.jpg" alt="">
                            <h3>Pendahuluan</h3>
                            <p style="text-align: justify;">
                                Usaha Mikro, Kecil, dan Menengah (UMKM) merupakan sektor penting dalam perekonomian Indonesia, termasuk dalam industri fashion. UMKM fashion berkontribusi terhadap penciptaan lapangan kerja, pertumbuhan ekonomi, dan identitas budaya.
                            </p>
                            <p style="text-align: justify;">
                                Namun, UMKM fashion di Indonesia juga menghadapi berbagai tantangan, salah satunya adalah pengaruh Pajak Penghasilan (PPh) terhadap cash flow mereka.Pajak Penghasilan (PPh) merupakan kewajiban yang tak terhindarkan bagi Usaha Mikro, Kecil, dan Menengah (UMKM) di industri fashion. Bagaimana PPh memengaruhi aliran kas (cash flow) merupakan perhatian serius bagi para pelaku usaha ini. Dalam industri fashion yang dinamis, setiap penerimaan dan pengeluaran memiliki dampak signifikan terhadap kelangsungan bisnis. PPh menjadi salah satu faktor yang dapat mengganggu keseimbangan aliran kas, terutama jika tidak dikelola dengan bijak.
                            </p>
                            <h3>Pengertian PPh dan Cash Flow</h3>
                            <p style="text-align: justify;">
                            PPh adalah pajak yang dikenakan atas penghasilan yang diterima oleh wajib pajak, termasuk UMKM. PPh terbagi menjadi dua jenis, yaitu PPh Final dan PPh Pasal 21/25. Cash flow adalah aliran kas yang masuk dan keluar dari suatu perusahaan. Cash flow yang positif menunjukkan bahwa perusahaan memiliki cukup uang untuk beroperasi dan berinvestasi, sedangkan cash flow yang negatif menunjukkan bahwa perusahaan mengalami kesulitan keuangan.
                            </p>
                            <h3>Pengaruh PPh terhadap Cash Flow UMKM</h3>
                            <p style="text-align: justify;">
                            PPh dapat memengaruhi cash flow UMKM dalam beberapa hal berikut:
                            <li>
                                Memahami Regulasi PPh: UMKM perlu memahami regulasi PPh yang berlaku agar mereka dapat menghitung dan membayar      pajak dengan benar.
                            </li>
                            <li>
                                Kewajiban Pembayaran Pajak: UMKM diwajibkan untuk membayar PPh secara berkala. Hal ini dapat membuat UMKM mengalami kesulitan keuangan, terutama jika mereka tidak memiliki cukup uang untuk membayar pajak.
                            </li>
                            <li>
                                Birokrasi: Proses pembayaran PPh bisa memakan waktu dan rumit. Hal ini dapat membuat UMKM kehilangan waktu dan uang.
                            </li>
                            </p>
                        </article>
                        <ul class="blog-meta ml-3">
                            <li style="margin-right: 50px; color: #01a0f9; width:250px;">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i> 15 Maret 2024
                                <i class="fa fa-user-o" aria-hidden="true"></i> Admin
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- .container end -->
    </div>

    <!-- Privacy Policy End -->
    <?php include './layout/footer.php'; ?>
</body>

</html>